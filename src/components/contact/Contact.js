import React, {useState} from 'react'
import nonameAvatar from './noname.png' 
import './contact.sass'

export const Contact = (props) => {

    const [cls, setCls] = useState('list-group-item hover')

    const checkAvatar = avatar => {
        if (avatar)
            return avatar
        else 
            return nonameAvatar
    }

    const selectContact = () => {
        if (cls === 'list-group-item hover') 
            setCls('list-group-item hover active')
        else
            setCls('list-group-item hover')
    }

    const selectAndPick = () => {
        selectContact(cls)
        props.pickedContact(props.id)
    }


    return (
        <li className={cls} onClick={() => selectAndPick()} >
            <span className='term'><img src={checkAvatar(props.avatar)} alt="contact avatar" /></span>
            <span className="term">{props.first_name}</span>
            &nbsp;
            <span className="term">{props.last_name}</span>
        </li>
    )
}
