import React, { useEffect, useState } from 'react';
import { Contact } from '../contact/Contact';
import { Spiner } from '../spinner/Spiner';
import './contactList.sass'

export const ContactList = () => {

    const [contacts, setContacts] = useState([])
    const [isLoad, setIsLoad] = useState(false)
    const [selected, setSelected] = useState(contacts)
    const [picked, setPicked] = useState([])

    const getAllContacts = async () => {
        const _apiBase = 'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json';
        const res = await fetch(_apiBase);
    
        if (!res.ok)
            throw new Error(`Could not fetch ${_apiBase} , received ${res.status}`);
        
        return await res.json()
    }

    const sortByLastName = promise => {
        promise
        .then(data => {
            setContacts(data.map(item => item).sort((a, b) => {
                if (a.last_name > b.last_name) 
                    return 1
                if (a.last_name < b.last_name)
                    return -1
                return 0
            })); 
            setIsLoad(true)
        })
    }
    
    const searchContact = value => {
        const selectedContacts = [];

        contacts.filter(element => {
            const valueLC = value.toLocaleLowerCase();
            const firstNameLC = element.first_name.toLowerCase();
            const lastNameLC = element.last_name.toLowerCase();

            if (firstNameLC.includes(valueLC) || lastNameLC.includes(valueLC))
                selectedContacts.push(element)
            
                return element
        });

        setSelected(selectedContacts)
    } 

    useEffect(() => {
        sortByLastName(getAllContacts());
    }, [])

    const pickedContact = id => {
        const newState = picked
        newState.push(id)
        setPicked(newState)
        console.log(`Selected contacts: ${picked}`)
    }

    const SelectedView =  () => {
        return (
            selected
            .map(item => <Contact 
                                key={item.id} 
                                first_name={item.first_name} 
                                last_name={item.last_name} 
                                avatar={item.avatar}
                                id={item.id}
                                pickedContact={pickedContact}
                                /> )
        )
    }

    return (
        <>
            <div className="contacts">
                <div className='d-flex'>
                    <input className="form-control w-100 d-inline-block" type="search" placeholder="Search contact..." 
                        onChange={(e) => searchContact(e.target.value)} 
                        />
                    <button className="btn btn-outline-success" onClick={(e) => searchContact('')} >Show contacts</button>
                </div>
                <ul className="list-group list-group-flush">
                    {isLoad ? <SelectedView /> : <Spiner />}
                </ul>
            </div>
        </>
    )
}
