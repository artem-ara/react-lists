import { ContactList } from '../contactsList/ContactList';
import { Nav } from '../nav/Nav';
import './App.sass';

function App() {
  return (
    <>
        <Nav />
        <ContactList />
    </>
  );
}

export default App;
