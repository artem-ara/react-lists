import React from 'react'
import './nav.sass'

export const Nav = () => {
    return (
        <nav className="navbar navbar-light d-flex justify-content-center">
            <h1>Contacts</h1>
        </nav>
    )
}
